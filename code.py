import random
import time

def print_board(list):
    print(f" -{list[1]}- -{list[2]}- -{list[3]}-\n -{list[4]}- -{list[5]}- -{list[6]}-\n -{list[7]}- -{list[8]}- -{list[9]}-")
    
def check_win_continue(list,winner):
    draw=True
    for j in range (numbers_of_field+1):
        if list[j]==j:
            draw=False

    if (list[1]==list[2]==list[3]) or (list[4]==list[5]==list[6]) or (list[7]==list[8]==list[9]) or (list[1]==list[4]==list[7]) or (list[2]==list[5]==list[8]) or (list[3]==list[6]==list[9]) or (list[1]==list[5]==list[9]) or (list[3]==list[5]==list[7]):
        print(f"Game is over, {winner} wins")
        return False
    elif draw:
        print("Game is draw")
        return False
    else:
        return True

numbers_of_field = 9

Field=[]
for x in range(numbers_of_field+1):
    Field.append(x)
Field[0]=""


print("What mode do you want to play 1 or 2 players. Write 1 or 2")
mode=input()
while mode!="1" and mode!="2":
    print("You wrote something wrong, please try again")
    mode=input()

continue_game=True 
flag=True

print_board(Field)
while continue_game & (mode=="1"):  
    flag_player=True
    while flag_player:
        choice=(input("Insert X in field number:"))
        if choice =='1' or choice =='2' or choice =='3' or choice =='4' or choice =='5' or choice =='6' or choice =='7' or choice =='8' or choice =='9':
            choice=int(choice)
            if Field[choice]!=choice:
                print("This field is already marked")
            else:
                Field[choice]="X"
                print_board(Field)
                flag_player=False
                continue_game = (check_win_continue(Field,"Player"))
    flag=True        
    while flag & continue_game:
        i=random.randint(1,9)
        if Field[i]==i:
            Field[i]="O"
            print(f"The computer fills the field: {i}")
            time.sleep(1)
            print_board(Field)
            continue_game = (check_win_continue(Field,"computer"))
            flag=False


while continue_game & (mode=="2"):  
    flag_player1=True
    while flag_player1:
        choice=(input("Insert X in field number:"))
        if choice =='1' or choice =='2' or choice =='3' or choice =='4' or choice =='5' or choice =='6' or choice =='7' or choice =='8' or choice =='9':
            choice=int(choice)
            if Field[choice]!=choice:
                print("This field is already marked")
            else:
                Field[choice]="X"
                print_board(Field)
                flag_player1=False
                continue_game = (check_win_continue(Field,"Player1"))
    flag_player2=True        
    while flag_player2 & continue_game:
        choice=(input("Insert O in field number:"))
        if choice =='1' or choice =='2' or choice =='3' or choice =='4' or choice =='5' or choice =='6' or choice =='7' or choice =='8' or choice =='9':
            choice=int(choice)
            if Field[choice]!=choice:
                print("This field is already marked")
            else:
                Field[choice]="O"
                print_board(Field)
                flag_player2=False
                continue_game = (check_win_continue(Field,"Player2"))
